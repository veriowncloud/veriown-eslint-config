module.exports = {
  extends: [
    './rules/best-practices.js',
    './rules/errors.js',
    './rules/node.js',
    './rules/style.js',
    './rules/variables.js',
    './rules/es6.js',
    './rules/imports.js',
    './rules/security.js'
  ],
  parserOptions: {
    ecmaVersion: 2019
  },
  rules: {}
};
